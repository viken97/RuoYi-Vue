package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ruoyi.common.constant.CharacterConstants;
import com.ruoyi.common.constant.EntityConstants;
import com.ruoyi.common.utils.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.service.ISysNoticeService;

/**
 * 公告 服务层实现
 *
 * @author ruoyi
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService
{
    @Autowired
    private SysNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeById(Long noticeId)
    {
        return noticeMapper.selectNoticeById(noticeId);
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice)
    {
        return noticeMapper.selectNoticeList(notice);
    }

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(SysNotice notice)
    {
        return noticeMapper.insertNotice(notice);
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(SysNotice notice)
    {
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long noticeId)
    {
        return Optional.ofNullable(noticeId).map(item -> this.noticeMapper.selectNoticeById(item)).map(item -> {
            Map<String, Object> params = EntityUtils.getModifiedAttribute();
            params.put("noticeId", item.getNoticeId());
            params.put(EntityConstants.PROP_DEL_FLAG_KEY, CharacterConstants.ONE_CHAR_VALUE);
            params.put(EntityConstants.PROP_STATUS_CODE_KEY, CharacterConstants.ONE_CHAR_VALUE);
            params.put(EntityConstants.PROP_MODIFIED_VERSION_KEY, item.getVor());
            return noticeMapper.deleteNoticeById(params);
        }).orElse(0);
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] noticeIds)
    {
        Map<String, Object> params = EntityUtils.getModifiedAttribute();
        params.put("noticeIds", Arrays.stream(noticeIds).filter(Objects::nonNull).collect(Collectors.toCollection(LinkedList::new)));
        params.put(EntityConstants.PROP_DEL_FLAG_KEY, CharacterConstants.ONE_CHAR_VALUE);
        params.put(EntityConstants.PROP_STATUS_CODE_KEY, CharacterConstants.ONE_CHAR_VALUE);
        return this.noticeMapper.deleteNoticeByIds(params);
    }
}
