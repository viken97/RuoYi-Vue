/**
 * 文件名: Dudu56ReflectUtils.java
 * 版    权: Copyright © 2013 - 2020 Vansee, Inc. All Rights Reserved
 * 描    述: &lt;描述&gt;
 * 修改人: WeiQiang.Fan
 * 修改时间: 2020/4/12
 * 跟踪单号: &lt;跟踪单号&gt;
 * 修改单号: &lt;修改单号&gt;
 * 修改内容: &lt;修改内容&gt;
 */
package com.ruoyi.common.utils;


import com.ruoyi.common.constant.CharacterConstants;
import com.ruoyi.common.constant.EntityConstants;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.reflect.ReflectUtils;
import org.apache.poi.ss.formula.functions.T;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 功能描述 : &lt;p&gt;&lt;/p&gt;
 *
 * @author vansee
 * @version [版本号,  2020/4/12]
 * @see [相关类/方法]
 * @since [产品/模板版本号]
 */
public class EntityUtils
{
    /**
     * 设置新增的默认值
     * EntityConstants.PROP_STATUS_CODE_KEY => statusCode
     * EntityConstants.PROP_DEL_FLAG_KEY => delFlag
     * EntityConstants.PROP_RECORD_VERSION_KEY => recordVersion
     * EntityConstants.PROP_CREATED_BY_KEY => createdBy
     * EntityConstants.PROP_CREATED_NAME_KEY => createdName
     * EntityConstants.PROP_MODIFIED_BY_KEY => modifiedBy
     * EntityConstants.PROP_MODIFIED_NAME_KEY => modifiedName
     *
     * @param object 对象实例
     * @param <T>
     */
    public static <T> void setCreatedAttribute(T object)
    {
        EntityUtils.setCreatedAttribute(object, false);
    }

    /**
     * 设置新增的默认值
     * EntityConstants.PROP_STATUS_CODE_KEY => statusCode
     * EntityConstants.PROP_DEL_FLAG_KEY => delFlag
     * EntityConstants.PROP_RECORD_VERSION_KEY => recordVersion
     * EntityConstants.PROP_CREATED_BY_KEY => createdBy
     * EntityConstants.PROP_CREATED_NAME_KEY => createdName
     * EntityConstants.PROP_CREATED_PATH_ID_KEY => createdPathId
     * EntityConstants.PROP_CREATED_PATH_NAME_KEY => createdPathName
     * EntityConstants.PROP_MODIFIED_BY_KEY => modifiedBy
     * EntityConstants.PROP_MODIFIED_NAME_KEY => modifiedName
     * EntityConstants.PROP_MODIFIED_PATH_ID_KEY => modifiedPathId
     * EntityConstants.PROP_MODIFIED_PATH_NAME_KEY => modifiedPathName
     *
     * @param object 对象实例
     * @pa <T>
     */
    public static <T> void setCreatedAttribute(T object, boolean organized)
    {
        List<String> propertyNames = (organized ?
                Arrays.asList(EntityConstants.PROP_STATUS_CODE_KEY,
                        EntityConstants.PROP_DEL_FLAG_KEY,
                        EntityConstants.PROP_RECORD_VERSION_KEY,
                        EntityConstants.PROP_CREATED_BY_KEY,
                        EntityConstants.PROP_CREATED_NAME_KEY,
                        EntityConstants.PROP_CREATED_PATH_ID_KEY,
                        EntityConstants.PROP_CREATED_PATH_NAME_KEY,
                        EntityConstants.PROP_MODIFIED_BY_KEY,
                        EntityConstants.PROP_MODIFIED_NAME_KEY,
                        EntityConstants.PROP_MODIFIED_PATH_ID_KEY,
                        EntityConstants.PROP_MODIFIED_PATH_NAME_KEY) :
                Arrays.asList(EntityConstants.PROP_STATUS_CODE_KEY,
                        EntityConstants.PROP_DEL_FLAG_KEY,
                        EntityConstants.PROP_RECORD_VERSION_KEY,
                        EntityConstants.PROP_CREATED_BY_KEY,
                        EntityConstants.PROP_CREATED_NAME_KEY,
                        EntityConstants.PROP_MODIFIED_BY_KEY,
                        EntityConstants.PROP_MODIFIED_NAME_KEY));
        propertyNames.stream().forEach(propertyName ->
        {
            Object value = getPropertyValue(propertyName);
            if (null != value)
            {
                ReflectUtils.invokeSetter(object, propertyName, value);
            }
        });
    }

    /**
     * 设置更新的默认值
     * EntityConstants.PROP_RECORD_VERSION_KEY => recordVersion
     * EntityConstants.PROP_MODIFIED_BY_KEY => modifiedBy
     * EntityConstants.PROP_MODIFIED_NAME_KEY => modifiedName
     *
     * @param object 对象实例
     * @param <T>
     */
    public static <T> void setModifiedAttribute(T object)
    {
        EntityUtils.setModifiedAttribute(object, false);
    }

    /**
     * 设置更新的默认值
     * EntityConstants.PROP_RECORD_VERSION_KEY => recordVersion
     * EntityConstants.PROP_MODIFIED_BY_KEY => modifiedBy
     * EntityConstants.PROP_MODIFIED_NAME_KEY => modifiedName
     * EntityConstants.PROP_MODIFIED_PATH_ID_KEY => modifiedPathId
     * EntityConstants.PROP_MODIFIED_PATH_NAME_KEY => modifiedPathName
     *
     * @param object 对象实例
     * @param <T>
     */
    public static <T> void setModifiedAttribute(T object, boolean organized)
    {
        List<String> propertyNames = (organized ?
                Arrays.asList(EntityConstants.PROP_RECORD_VERSION_KEY,
                        EntityConstants.PROP_MODIFIED_BY_KEY,
                        EntityConstants.PROP_MODIFIED_NAME_KEY,
                        EntityConstants.PROP_MODIFIED_PATH_ID_KEY,
                        EntityConstants.PROP_MODIFIED_PATH_NAME_KEY) :
                Arrays.asList(EntityConstants.PROP_RECORD_VERSION_KEY,
                        EntityConstants.PROP_MODIFIED_BY_KEY,
                        EntityConstants.PROP_MODIFIED_NAME_KEY));
        propertyNames.stream().forEach(propertyName ->
        {
            Object value = getPropertyValue(propertyName);
            if (null != value)
            {
                ReflectUtils.invokeSetter(object, propertyName, value);
            }
        });
    }

    /**
     * 获得更新的默认值Map集合
     * <p>
     * EntityConstants.PROP_RECORD_VERSION_KEY => recordVersion
     * EntityConstants.PROP_MODIFIED_BY_KEY => modifiedBy
     * EntityConstants.PROP_MODIFIED_NAME_KEY => modifiedName
     *
     * @return 更新的默认值Map集合
     */
    public static Map<String, Object> getModifiedAttribute()
    {
        return EntityUtils.getModifiedAttribute(false);
    }

    /**
     * 获得更新的默认值Map集合
     * <p>
     * EntityConstants.PROP_RECORD_VERSION_KEY => recordVersion
     * EntityConstants.PROP_MODIFIED_BY_KEY => modifiedBy
     * EntityConstants.PROP_MODIFIED_NAME_KEY => modifiedName
     * EntityConstants.PROP_MODIFIED_PATH_ID_KEY => modifiedPathId
     * EntityConstants.PROP_MODIFIED_PATH_NAME_KEY => modifiedPathName
     *
     * @return 更新的默认值Map集合
     */
    public static Map<String, Object> getModifiedAttribute(boolean organized)
    {
        List<String> propertyNames = (organized ?
                Arrays.asList(EntityConstants.PROP_RECORD_VERSION_KEY,
                        EntityConstants.PROP_MODIFIED_BY_KEY,
                        EntityConstants.PROP_MODIFIED_NAME_KEY,
                        EntityConstants.PROP_MODIFIED_PATH_ID_KEY,
                        EntityConstants.PROP_MODIFIED_PATH_NAME_KEY) :
                Arrays.asList(EntityConstants.PROP_RECORD_VERSION_KEY,
                        EntityConstants.PROP_MODIFIED_BY_KEY,
                        EntityConstants.PROP_MODIFIED_NAME_KEY));
        return propertyNames.stream().collect(Collectors.toMap(propertyName -> propertyName,
                propertyName -> getPropertyValue(propertyName)));
    }

    /**
     * 设置权限字符条件
     *
     * @param object   对象实例
     * @param beanName DataScopeCustomizer Instance BeanName
     * @param <T>
     */
    public static <T extends BaseEntity> void setDataScopeValue(T object, String beanName)
    {
        /*DataScopeCustomizer customizer = (null != beanName && 0 < beanName.length() ? SpringUtils.getBean(beanName) :
                SpringUtils.getBean(DataScopeCustomizer.class));
        if (null != customizer)
        {
            SysUser user = SecurityUtils.getLoginUser().getUser();
            if (!user.isAdmin())
            {
                object.getParams().put("dataScope", customizer.customize(user));
            }
        }*/
    }

    private static Object getPropertyValue(String propertyName)
    {
        if (EntityConstants.PROP_CREATED_BY_KEY.equals(propertyName)
                || EntityConstants.PROP_MODIFIED_BY_KEY.equals(propertyName))
        {
            return SecurityUtils.getUserId();
        }
        else if (EntityConstants.PROP_CREATED_NAME_KEY.equals(propertyName)
                || EntityConstants.PROP_MODIFIED_NAME_KEY.equals(propertyName))
        {
            return SecurityUtils.getNickName();
        }
        else if (EntityConstants.PROP_CREATED_PATH_ID_KEY.equals(propertyName)
                || EntityConstants.PROP_MODIFIED_PATH_ID_KEY.equals(propertyName))
        {
            return SecurityUtils.getDeptPathId();
        }
        else if (EntityConstants.PROP_CREATED_PATH_NAME_KEY.equals(propertyName)
                || EntityConstants.PROP_MODIFIED_PATH_NAME_KEY.equals(propertyName))
        {
            return SecurityUtils.getDeptPathName();
        }
        else if (EntityConstants.PROP_DEL_FLAG_KEY.equals(propertyName)
                || EntityConstants.PROP_STATUS_CODE_KEY.equals(propertyName))
        {
            return CharacterConstants.ZERO_CHAR_VALUE;
        }
        else if (EntityConstants.PROP_RECORD_VERSION_KEY.equals(propertyName)
                || EntityConstants.PROP_MODIFIED_VERSION_KEY.equals(propertyName))
        {
            return System.nanoTime();
        }
        else
        {
            return null;
        }
    }
}