package com.ruoyi.common.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.CustomException;

/**
 * 安全服务工具类
 * 
 * @author ruoyi
 */
public class SecurityUtils
{

    /**
     * 获取用户昵称
     **/
    public static String getNickName()
    {
        try
        {
            return getLoginUser().getUser().getNickName();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取用户账户ID
     **/
    public static Long getUserId()
    {
        try
        {
            return getLoginUser().getUser().getUserId();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取用户账户
     **/
    public static String getUsername()
    {
        try
        {
            return getLoginUser().getUsername();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获得当前登录用户的部门ID
     *
     * @return 当前登录用户的部门ID
     */
    public static Long getDeptId()
    {
        try
        {
            return getLoginUser().getUser().getDept().getDeptId();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户归属组织异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获得当前登录用户的部门ID路径
     *
     * @return 当前登录用户的部门ID路径
     */
    public static String getDeptPathId()
    {
        try
        {
            return ""; //TODO getLoginUser().getUser().getDept().getDeptPathId();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户归属组织异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获得当前登录用户的部门名称路径
     *
     * @return 当前登录用户的部门名称路径
     */
    public static String getDeptPathName()
    {
        try
        {
            return ""; //TODO getLoginUser().getUser().getDept().getDeptPathName();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户归属组织异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取用户
     **/
    public static LoginUser getLoginUser()
    {
        try
        {
            return (LoginUser) getAuthentication().getPrincipal();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户信息异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication()
    {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword 真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 是否为管理员
     * 
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }
}
