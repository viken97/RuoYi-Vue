/**
 * 文件名: EntityConstants.java
 * 版    权: Copyright © 2013 - 2020 Vansee, Inc. All Rights Reserved
 * 描    述: &lt;描述&gt;
 * 修改人: WeiQiang.Fan
 * 修改时间: 2020/4/12
 * 跟踪单号: &lt;跟踪单号&gt;
 * 修改单号: &lt;修改单号&gt;
 * 修改内容: &lt;修改内容&gt;
 */
package com.ruoyi.common.constant;

/**
 * 功能描述 : &lt;p&gt;&lt;/p&gt;
 * @author vansee
 * @version [版本号,  2020/4/12]
 * @see [相关类/方法]
 * @since [产品/模板版本号]
 */
public interface EntityConstants
{
    String PROP_STATUS_CODE_KEY = "statusCode";

    String PROP_DEL_FLAG_KEY = "delFlag";

    String PROP_CREATED_BY_KEY = "createdBy";

    String PROP_CREATED_NAME_KEY = "createdName";

    String PROP_CREATED_PATH_ID_KEY = "createdPathId";

    String PROP_CREATED_PATH_NAME_KEY = "createdPathName";

    String PROP_MODIFIED_BY_KEY = "modifiedBy";

    String PROP_MODIFIED_NAME_KEY = "modifiedName";

    String PROP_MODIFIED_PATH_ID_KEY = "modifiedPathId";

    String PROP_MODIFIED_PATH_NAME_KEY = "modifiedPathName";

    String PROP_RECORD_VERSION_KEY = "vor";

    String PROP_MODIFIED_VERSION_KEY = "vom";
}