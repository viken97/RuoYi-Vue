/**
 * 文件名: CharacterConstants.java
 * 版    权: Copyright © 2013 - 2020 Vansee, Inc. All Rights Reserved
 * 描    述: &lt;描述&gt;
 * 修改人: WeiQiang.Fan
 * 修改时间: 2020/4/12
 * 跟踪单号: &lt;跟踪单号&gt;
 * 修改单号: &lt;修改单号&gt;
 * 修改内容: &lt;修改内容&gt;
 */
package com.ruoyi.common.constant;

/**
 * 功能描述 : &lt;p&gt;&lt;/p&gt;
 * @author vansee
 * @version [版本号,  2020/4/12]
 * @see [相关类/方法]
 * @since [产品/模板版本号]
 */
public interface CharacterConstants
{

    /** 零字符串值*/
    String ZERO_CHAR_VALUE = "0";

    /** 壹字符串值*/
    String ONE_CHAR_VALUE = "1";
}