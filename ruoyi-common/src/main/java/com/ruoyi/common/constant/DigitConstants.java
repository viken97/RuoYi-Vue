/**
 * 文件名: StatusCode.java
 * 版    权: Copyright © 2013 - 2019 Vansee, Inc. All Rights Reserved
 * 描    述: &lt;描述&gt;
 * 修改人: WeiQiang.Fan
 * 修改时间: 2019-09-23
 * 跟踪单号: &lt;跟踪单号&gt;
 * 修改单号: &lt;修改单号&gt;
 * 修改内容: &lt;修改内容&gt;
 */
package com.ruoyi.common.constant;

/**
 * 功能描述 : &lt;p&gt;&lt;/p&gt;
 * @author vansee
 * @version [版本号,  2019-09-23]
 * @see [相关类/方法]
 * @since [产品/模板版本号]
 */
public interface DigitConstants
{
    int ONE_CODE_NEGATIVE_VALUE = -1;

    int ZERO_CODE_VALUE = 0;

    int ONE_CODE_VALUE = 1;

    int TWO_CODE_VALUE = 2;

    int THREE_CODE_VALUE = 3;

    int FOUR_CODE_VALUE = 4;

    int FIVE_CODE_VALUE = 5;

    int SIX_CODE_VALUE = 6;

    int SEVEN_CODE_VALUE = 7;

    int EIGHT_CODE_VALUE = 8;

    int NINE_CODE_VALUE = 9;

    int TEN_CODE_VALUE = 10;

    int SIXTEEN_CODE_VALUE = 16;

    int TWENTY_CODE_VALUE = 20;

    int THIRTY_CODE_VALUE = 30;

    int THIRTY_THREE_CODE_VALUE = 33;

    int FORTY_CODE_VALUE = 40;

    int FORTY_FOUR_CODE_VALUE = 44;

    int FIFTY_CODE_VALUE = 50;

    int FIFTY_FIVE_CODE_VALUE = 55;

    int SIXTY_CODE_VALUE = 60;

    int SIXTY_SIX_CODE_VALUE = 66;

    int SEVENTY_CODE_VALUE = 70;

    int SEVENTY_SEVEN_CODE_VALUE = 77;

    int EIGHTY_CODE_VALUE = 80;

    int EIGHTY_EIGHT_CODE_VALUE = 88;

    int NINETY_CODE_VALUE = 90;

    int NINETY_NINE_CODE_VALUE = 99;

    int HUNDRED_CODE_VALUE = 100;
}