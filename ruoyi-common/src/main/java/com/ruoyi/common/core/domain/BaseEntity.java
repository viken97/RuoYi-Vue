package com.ruoyi.common.core.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;

/**
 * Entity基类
 * 
 * @author ruoyi
 */
public class BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 租户ID路径 */
    private String tenantIdPath;

    /** 租户名称路径 */
    private String tenantNamePath;

    /** 搜索值 */
    private String searchValue;

    /** 创建者ID */
    @JsonDeserialize(using = StringDeserializer.class)
    private Long createdBy;

    /** 创建者 */
    private String createdName;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdTime;

    /** 更新者ID */
    @JsonDeserialize(using = StringDeserializer.class)
    private Long modifiedBy;

    /** 更新者 */
    private String modifiedName;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date modifiedTime;

    /** 备注 */
    private String remark;

    /** 记录版本号 Version of Record*/
    @JsonDeserialize(using = StringDeserializer.class)
    private Long vor;

    /** 更新记录版本号 Version for Modified*/
    private Long vom;

    /** 请求参数 */
    private Map<String, Object> params;

    public String getTenantIdPath()
    {
        return tenantIdPath;
    }

    public void setTenantIdPath(String tenantIdPath)
    {
        this.tenantIdPath = tenantIdPath;
    }

    public String getTenantNamePath()
    {
        return tenantNamePath;
    }

    public void setTenantNamePath(String tenantNamePath)
    {
        this.tenantNamePath = tenantNamePath;
    }

    public String getSearchValue()
    {
        return searchValue;
    }

    public void setSearchValue(String searchValue)
    {
        this.searchValue = searchValue;
    }

    public Long getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedName()
    {
        return createdName;
    }

    public void setCreatedName(String createdName)
    {
        this.createdName = createdName;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedName()
    {
        return modifiedName;
    }

    public void setModifiedName(String modifiedName)
    {
        this.modifiedName = modifiedName;
    }

    public Date getModifiedTime()
    {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public Long getVor()
    {
        return vor;
    }

    public void setVor(Long vor)
    {
        this.vor = vor;
    }

    public Long getVom()
    {
        return vom;
    }

    public void setVom(Long vom)
    {
        this.vom = vom;
    }

    public Map<String, Object> getParams()
    {
        if (params == null)
        {
            params = new HashMap<>();
        }
        return params;
    }

    public void setParams(Map<String, Object> params)
    {
        this.params = params;
    }
}
